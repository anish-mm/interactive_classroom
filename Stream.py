import struct
import cv2
from mss import mss
import numpy as np
import pyaudio
class Stream:
	def __init__(self, mode, filename=''):
		self.mode = mode
		if mode == 0:
			self.filename = filename
			try:
				self.file = open(filename, 'rb')
				print '-'*60 +  "\nVideo file : |" + filename +  "| read\n" + '-'*60
			except:
				raise IOError
		elif mode  == 1:
			self.vid = cv2.VideoCapture(0)
		elif mode == 2:
			self.sc = mss()
		elif mode == 3:
			self.chunk = 8500
			self.FORMAT = pyaudio.paInt16
			self.channels = 1
			self.rate= 44100
			self.time_recorded = 0.033
			self.p = pyaudio.PyAudio()

			self.stream = self.p.open(format=self.FORMAT,
			                channels=self.channels,
			                rate=self.rate,
			                input=True,
			                frames_per_buffer=self.chunk)

		self.frameNum = 0

	def read_next_frame2(self):
		self.frameNum += 1
		monitor = {'top':0, 'left':0, 'width':1366, 'height':768}
		with mss() as sc:
			img = np.array(sc.grab(monitor))
			img = cv2.resize(img, (0,0), fx=0.8, fy=0.8)
			frame = cv2.imencode('.jpeg', img,[1, 30])[1].tostring()
			return frame

	def read_next_frame(self):
		if self.mode == 0:
			return self.read_next_frame0()
		elif self.mode == 1:
			return self.read_next_frame1()
		elif self.mode == 2:
			return self.read_next_frame2()
		elif self.mode == 3:
			return self.read_next_frame3()

	def read_next_frame0(self):
		data = self.file.read(5)
		data = bytearray(data)

		data_int = (data[0] - 48) * 10000 + (data[1] - 48) * 1000 + (data[2] - 48) * 100 + (data[3] - 48) * 10 + (data[4] - 48)

		final_data_int = data_int

		if data:

			framelength = final_data_int

			frame = self.file.read(framelength)
			if len(frame) != framelength:
				raise ValueError('incomplete frame data')

			self.frameNum += 1

			print '-'*10 + "\nNext Frame (#" + str(self.frameNum) + ") length:" + str(framelength) + "\n" + '-'*10

			return frame
			
	def read_next_frame3(self):
		self.frameNum += 1
		data  = self.stream.read(self.chunk)

		return data

	def read_next_frame1(self):
		while(self.vid.isOpened()):
			ret, img = self.vid.read()
			#img = cv2.resize(img, (0,0), fx=0.5, fy=0.5)
			frame = cv2.imencode('.jpeg', img,[1, 50])[1].tostring()
			if ret == True:
				self.frameNum += 1
				return frame

	def release_cam(self):
		self.vid.release()

	def release_mic(self):
		self.stream.stop_stream()
		self.stream.close()
		self.p.terminate()
		self.s.close()
	def get_frame_number(self):
                return self.frameNum
