import pygame as pg
import sys
from pygame.locals import *
import cv2
import threading
import socket
from RtpPacket import RtpPacket
import numpy as np
import pyaudio

class Client:
    INIT = 0
    READY = 1
    PLAYING = 2
    state = INIT

    SETUP = 0
    PLAY = 1
    PAUSE = 2
    TEARDOWN = 3

    def __init__(self, serveraddr, serverport, rtpport, filename, auport):
        self.server_addr = serveraddr
        self.server_port = int(serverport)
        self.rtp_port = int(rtpport)
        self.file_name = filename
        self.rtsp_seq = 0
        self.session_id = 0
        self.requestSent = -1
        self.teardown_acked = 0
        self.connectToServer()
        self.frame_number = 0
        self.au_port = int(auport)
        self.au_frame_number = 0
        self.rtp_socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.aud_socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.createDisplay()

    def createDisplay(self):
        pg.init()
        self.width = 800
        self.height = 400
        self.createWindow()
        threading.Thread(target=self.manageClient).start()
        threading.Thread(target=self.manageAudClient).start()
        self.sendRtspRequest(self.SETUP)


    def connectToServer(self):
        # connect to server and start RTSP / TCP.
        self.rtsp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.rtsp_socket.connect((self.server_addr, self.server_port))
        except:
            print "Connection to %s failed" %self.server_addr

    def createWindow(self):
        self.win = pg.display.set_mode((self.width, self.height), pg.RESIZABLE)
        self.win.fill((255, 255, 255))
        pg.display.set_caption("Video player")

    def playMovie(self):
        if self.state == self.READY :
            print "playing movie..."
            self.sendRtspRequest(self.PLAY)

    def pauseMovie(self):
        if self.state == self.PLAYING :
            print "pausing movie..."
            self.sendRtspRequest(self.PAUSE)

    def exitMovie(self):
        self.sendRtspRequest(self.TEARDOWN)
        print '_'*60
        print "stopping movie..."
        sys.exit()

    def manageAudClient(self):
        chunk = 1024
        FORMAT = pyaudio.paInt16
        channels = 1
        rate = 44100
        record_sec = 4
        WIDTH = 2
        frames = []
        p = pyaudio.PyAudio()
        stream = p.open(format=p.get_format_from_width(WIDTH),
                        channels=channels,
                        rate=rate,
                        output=True,
                        frames_per_buffer=chunk)
        while True:
            try:
                data,addr = self.aud_socket.recvfrom(91480)
                if data:
                    rtp_packet = RtpPacket()
                    rtp_packet.decode(data)
                    print "||Received Rtp Packet #" + str(rtp_packet.seqNum()) + "|| "

                    try:
                        if self.au_frame_number + 1 != rtp_packet.seqNum():
                            print '!'*60 + "\nPACKET LOSS for audio\n" + '!'*60
                        current_frame_number = rtp_packet.seqNum()
                    except:
                        print "seqNum() error"
                        print '-'*60
                        traceback.print_exc(file=sys.stdout)
                        print '-'*60

                    if current_frame_number > self.au_frame_number: # Discard the late packet
                        self.au_frame_number = current_frame_number
                        stream.write(rtp_packet.getPayload())

            except:
                print "Didn`t receive data!"

                if self.teardown_acked == 1:
                    self.aud_socket.close()
                    break

    def manageClient(self):
        '''
            keep updating the display window. if video playing, put its frame in.
        '''
        clock = pg.time.Clock()
        while True:
            time = clock.tick(30)
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    self.exitMovie()
                if event.type == pg.VIDEORESIZE:
                    self.width, self.height = event.size
                    self.createWindow()

                if event.type == pg.MOUSEBUTTONDOWN:
                    mouse_pos = event.pos
                    if self.play_rect.collidepoint(mouse_pos):
                        self.playMovie()
                    elif self.pause_rect.collidepoint(mouse_pos):
                        self.pauseMovie()
                    elif self.exit_rect.collidepoint(mouse_pos):
                        self.exitMovie()
                self.width, self.height = self.win.get_size()

                if self.state == self.PLAYING :
                    try:
                        data, addr = self.rtp_socket.recvfrom(40960)
                        if data:
                            self.rtp_packet = RtpPacket()
                            self.rtp_packet.decode(data)

                            try:
                                if self.frame_number + 1 != self.rtp_packet.seqNum():
                                    print '!'*20+'packet loss for video'+'!'*20
                                current_frame_number = self.rtp_packet.seqNum()
                            except:
                                print "seqNum() error"
                                traceback.print_exc(file=sys.stdout)
                            if current_frame_number > self.frame_number :
                                 self.frame_number = current_frame_number
                                 self.updateMovie(self.rtp_packet.getPayload())
                                 w, h = self.img.get_size()
                                 self.img=pg.transform.scale(self.img, (int((h/(self.height*0.9))*self.width), int(self.height*0.9)))
                                 self.win.blit(self.img, (0,0))
                    except:
                        print "Din't receive data"

                        if self.teardown_acked == 1:
                            self.rtp_socket.shutdown(socket.SHUT_RDWR)
                            self.rtp_socket.close()
                            break
                self.play_button = pg.Surface((80, min(self.height*0.06, 30)))
                self.play_position = (4, self.height*0.92)
                self.play_rect = self.play_button.get_rect(topleft = self.play_position)
                self.play_button.fill((0,0,255))
                # create pause button
                self.pause_button = pg.Surface((80, min(self.height*0.06, 30)))
                self.pause_position = (88, self.height*0.92)
                self.pause_rect = self.pause_button.get_rect(topleft = self.pause_position)
                self.pause_button.fill((0, 255,0))
                # create exit button
                self.exit_button = pg.Surface((80, min(self.height*0.06, 30)))
                self.exit_position = (172, self.height*0.92)
                self.exit_rect = self.exit_button.get_rect(topleft = self.exit_position)
                self.exit_button.fill((255,0,0))

                self.win.blit(self.play_button, self.play_position)
                self.win.blit(self.pause_button, self.pause_position)
                self.win.blit(self.exit_button, self.exit_position)

                pg.display.flip()

    def updateMovie(self, img_file):
        try:
            np_array = np.fromstring(self.rtp_packet.getPayload(), np.uint8)
            image = cv2.imdecode(np_array, 1)
            print image.shape
            h, w, _ = image.shape
            self.img = pg.image.frombuffer(image.tostring(),(w, h), "RGB")

        except:
            print "photo error"

    def sendRtspRequest(self, request_code):
        # Setup request
        if request_code == self.SETUP and self.state == self.INIT:
            threading.Thread(target=self.recvRtspReply).start()
            self.rtsp_seq = 1
            request = "SETUP " + str(self.file_name) + "\n" + str(self.rtsp_seq) + "\n" + " RTSP/1.0 RTP/UDP " + str(self.rtp_port)+ ' '+str(self.au_port)

            self.rtsp_socket.send(request)
            # Keep track of the sent request.
            self.request_sent = self.SETUP

        # Play request
        elif request_code == self.PLAY and self.state == self.READY:
            self.rtsp_seq = self.rtsp_seq + 1
            request = "PLAY " + "\n" + str(self.rtsp_seq)

            self.rtsp_socket.send(request)
            print '_'*60 + "\nPLAY request sent to Server...\n" + '_'*60
            self.request_sent = self.PLAY

        # Pause request
        elif request_code == self.PAUSE and self.state == self.PLAYING:
            self.rtsp_seq = self.rtsp_seq + 1
            request = "PAUSE " + "\n" + str(self.rtsp_seq)
            self.rtsp_socket.send(request)
            print '_'*60 + "\nPAUSE request sent to Server...\n" + '_'*60
            # Keep track of the sent request.
            self.request_Sent = self.PAUSE

        # Teardown request
        elif request_code == self.TEARDOWN and not self.state == self.INIT:
            self.rtsp_seq = self.rtsp_seq + 1
            request = "TEARDOWN " + "\n" + str(self.rtsp_seq)
            self.rtsp_socket.send(request)
            print '_'*60 + "\nTEARDOWN request sent to Server...\n" + '_'*60
            self.request_sent = self.TEARDOWN

        else:
            return

    def openRtpPort(self):
        self.rtp_socket.settimeout(0.5)
        try:
            self.rtp_socket.bind(('', self.rtp_port))
            print "vid rtp port bind success"
            self.aud_socket.bind(('', self.au_port))
            print "aud rtp port bind success"
        except:
            print "rtp bind error"+'!'*30

    def recvRtspReply(self):
        """Receive RTSP reply from the server."""
        while True:
            reply = self.rtsp_socket.recv(1024)

            if reply:
                self.parseRtspReply(reply)

            # Close the RTSP socket upon requesting Teardown
            if self.request_sent == self.TEARDOWN:
                self.rtsp_socket.shutdown(socket.SHUT_RDWR)
                self.rtsp_socket.close()
                self.aud_socket.close()
                break

    def parseRtspReply(self, data):
        print "Parsing Received Rtsp data..."

        """Parse the RTSP reply from the server."""
        lines = data.split('\n')
        seq_num = int(lines[1].split(' ')[1])

        # Process only if the server reply's sequence number is the same as the request's
        if seq_num == self.rtsp_seq:
            session = int(lines[2].split(' ')[1])
            # New RTSP session ID
            if self.session_id == 0 :
                self.session_id = session

            # Process only if the session ID is the same
            if self.session_id == session:
                if int(lines[0].split(' ')[1]) == 200:
                    if self.request_sent == self.SETUP:
                        # update RTSP state.
                        print "Updating RTSP state..."
                        self.state = self.READY
                        # Open RTP port.
                        print "Setting Up RtpPort for Video Stream"
                        self.openRtpPort()

                    elif self.request_sent == self.PLAY:
                        self.state = self.PLAYING
                        print '_'*60 + "\nClient is PLAYING...\n" + '_'*60
                    elif self.request_sent == self.PAUSE:
                        self.state = self.READY

                    elif self.request_sent == self.TEARDOWN:
                        self.teardown_acked = 1
