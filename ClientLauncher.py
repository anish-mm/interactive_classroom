import sys
from Client import Client

if __name__ == "__main__":
    try:
        serverAddr = sys.argv[1]
        serverPort = sys.argv[2]
        rtpPort = sys.argv[3]
        auport = sys.argv[4]
        fileName = sys.argv[5]
    except:
        print "wrong usage"

    app = Client(serverAddr, serverPort, rtpPort, fileName, auport)
