import sys
import socket
from ClientHandler import ClientHandler
class Server:

    def main(self):
        try:
            SERVER_PORT = int(sys.argv[1])
        except:
            print "[Usage: Server.py Server_port]\n"
            sys.exit(0)
        try:
            rtspSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            rtspSocket.bind(('', SERVER_PORT))
            print "RTSP Listing incoming request..."
            rtspSocket.listen(5)
        except:
            print "Cannot open socket"
            sys.exit(0)

        while True:
            try:
                sock, address = rtspSocket.accept()
                client_info = {}
                client_info['rtsp_socket'] = sock
                client_info['address'] = address
                ClientHandler(client_info).start()
            except KeyboardInterrupt:
                rtspSocket.close()
                sys.exit(0)



if __name__ == "__main__":
    (Server()).main()
