import threading
import socket
import time
from Stream import Stream
from random import randint
from RtpPacket import RtpPacket
class ClientHandler:
    SETUP = 'SETUP'
    PLAY = 'PLAY'
    PAUSE = 'PAUSE'
    TEARDOWN = 'TEARDOWN'

    INIT = 0
    READY = 1
    PLAYING = 2
    state = INIT

    OK_200 = 0
    FILE_NOT_FOUND_404 = 1
    CON_ERR_500 = 2

    def __init__(self, client_info):
        self.client_info = client_info

    def start(self):
        threading.Thread(target = self.recv_rtsp_requests).start()

    def recv_rtsp_requests(self):
        sock = self.client_info['rtsp_socket']
        while 1:
            data = sock.recv(256)
            if data:
                print "Request received\n"
                self.process_rtsp_request(data)

    def make_rtp_packet(self, payload, frame_number):
        version = 2
        padding = 0
        extension = 0
        cc = 0
        marker = 0
        pt = 26
        seqnum = frame_number
        ssrc = 0
        rtpPacket = RtpPacket()
        rtpPacket.encode(version, padding, extension, cc, seqnum, marker, pt, ssrc, payload)
        return rtpPacket.getPacket()

    def process_rtsp_request(self, input_data):
        request = input_data.split('\n')
        line1  = request[0].split(' ')
        req_type = line1[0]
        file_name = line1[1]
        seq = request[1].split(' ')
        if req_type == self.SETUP:
            if self.state == self.INIT:
                print "SETUP Request received\n"
                try:
                    if file_name.count('.') > 0:
                        self.client_info['video_stream'] = Stream(0,file_name)
                    elif file_name == 'camera':
                        self.client_info['video_stream'] = Stream(1)
                        self.client_info['audio_stream'] = Stream(3)
                    elif file_name == 'screen':
                        self.client_info['video_stream'] = Stream(2)
                    elif file_name == 'mic':
                        self.client_info['video_stream'] = Stream(3)
                    else:
                        return
                    self.state = self.READY
                except IOError:
                    self.rtsp_reply(self.FILE_NOT_FOUND_404, seq[0])

                self.client_info['session'] = randint(100000, 999999)
                self.rtsp_reply(self.OK_200, seq[0])
                self.client_info['rtp_port'] = request[2].split(' ')[3]
                self.client_info['aud_port'] = request[2].split(' ')[4]
        elif req_type == self.PLAY:
            if self.state == self.READY:
                print "PLAY Request received\n"
                self.state = self.PLAYING
                self.client_info["rtp_socket"] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                self.rtsp_reply(self.OK_200, seq[0])
                self.client_info['event'] = threading.Event()
                self.client_info['sender'] = threading.Thread(target=self.send_rtp_packets)
                self.client_info['sender'].start()

                if self.client_info.has_key('audio_stream'):
                    self.client_info["rtp_aud_socket"] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    self.client_info['ausender'] = threading.Thread(target=self.send_a_rtp_packets)
                    self.client_info['ausender'].start()

            elif self.state == self.PAUSE:
                print "RESUME Request received\n"
                self.state = self.PLAYING

        elif req_type == self.PAUSE:
            if self.state == self.PLAYING:
                print "PAUSE Request received\n"
                self.state = self.READY
                self.client_info['event'].set()
                self.rtsp_reply(self.OK_200, seq[0])
        elif req_type == self.TEARDOWN:
            print "TEARDOWN Request received\n"
            self.client_info['event'].set()
            self.rtsp_reply(self.OK_200, seq[0])
            self.client_info['rtp_socket'].close()
            if self.client_info.has_key('audio_stream'):
                self.client_info['rtp_aud_socket'].close()
            if self.client_info['video_stream'].mode == 1:
                self.client_info['video_stream'].release_cam()

    def send_a_rtp_packets(self):
        while 1:
            if self.client_info['event'].isSet():
                break
            data = self.client_info['audio_stream'].read_next_frame()
            if data:
                frame_no = self.client_info['audio_stream'].get_frame_number()

                try:
                    port = int(self.client_info['aud_port'])
                    address = self.client_info['address'][0]
                    packet = self.make_rtp_packet(data, frame_no)
                    self.client_info['rtp_aud_socket'].sendto(packet,(address,port))

                except:
                   print "Audio Connection Error"
    def send_rtp_packets(self):
        while 1:
            if self.client_info['event'].isSet():
                break
            data = self.client_info['video_stream'].read_next_frame()
            if data:
                frame_no = self.client_info['video_stream'].get_frame_number()

                try:
                    port = int(self.client_info['rtp_port'])
                    address = self.client_info['address'][0]
                    packet = self.make_rtp_packet(data, frame_no)
                    self.client_info['rtp_socket'].sendto(packet,(address,port))
                    if self.client_info['video_stream'].mode != 3:
                        time.sleep(0.033)
                except:
                    print "Video Connection Error "

    def rtsp_reply(self, code, seq):

        if code == self.OK_200:
            reply = 'RTSP/1.0 200 OK\nCSeq: ' + seq + '\nSession: ' + str(self.client_info['session'])
            socket = self.client_info['rtsp_socket']
            socket.send(reply)

        elif code == self.FILE_NOT_FOUND_404:
            print "404 NOT FOUND"
        elif code == self.CON_ERR_500:
            print "500 CONNECTION ERROR"
